require 'net/http'
require 'json'

class GenderizeWrapper
  class << self
    SERVICE_BASE_URL = 'https://api.genderize.io'
    @@cache = nil
    @@previous_names = nil

    def gender_of(names)
      return @@cache if names == @@previous_names
      @@previous_names= names
      @@cache = request(create_query_string(names))
    end

    private

    def create_query_string(params)
      if params.is_a?(Array)
        multiple_name_param(params)
      else
        single_name_param(params)
      end
    end

    def single_name_param(name)
      "name=#{name}"
    end

    def multiple_name_param(names)
      query_string = ''
      names.each_with_index do |name, index|
        query_string << "name[#{index}]=#{name}&"
      end
      query_string
    end

    def request(query_string)
      uri = URI("#{SERVICE_BASE_URL}/?#{query_string}")
      response = Net::HTTP.get(uri)
      JSON.parse(response)
    end
  end
end
