Gem::Specification.new do |s|
  s.author = 'Danilo Barion Nogueira'
  s.files = ['lib/genderize_wrapper.rb']
  s.name = 'genderize_wrapper'
  s.version = '0.0.2'
  s.required_ruby_version = '>= 1.9'
  s.date = '2017-10-18'
  s.summary = 'Wrapper for Genderize.io'
  s.description = 'Get gender info from first name(s) using Genderize.io API.'
  s.email = 'danilo.barion@gmail.com'
  s.license = 'MIT'
  s.homepage = 'http://rubygems.org/gems/genderize_wrapper'
end
